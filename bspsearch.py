import re

REGEX_WEAPON = re.compile('\"classname\" \".*weapon.*\"')
REGEX_JAIL = re.compile('.*jail.*')
REGEX_SECRET = re.compile('.*secret.*')


class BSPsearch:

    def __init__(self, input):

        self.errors = {
            "jail": [], "weapon": [], "secret": [], "game_player_equip": [], "trigger_hurt": [],
            "env_spritetrail": [], "logic_auto": [], "logic_case": [], "logic_relay": [],
            "logic_timer": [], "env_shake": [], "func_rotating": [], "func_button": [],
            "func_rot_button": [], "point_servercommand": [], "func_breakable": [],
            "env_hudhint": [], "game_text": [], "func_door": [], "func_movelinear": [],
            "player_speedmod": []
            }
        self.input = input
        self.lines = self.input.split("\n")

    def search(self):
        for linenum, line in enumerate(self.lines):

            if REGEX_JAIL.search(line):
                self.errors["jail"].append(linenum + 1)

            if REGEX_WEAPON.search(line):
                self.errors["weapon"].append(linenum + 1)

            if REGEX_SECRET.search(line):
                self.errors["secret"].append(linenum + 1)

            if "\"classname\" \"game_player_equip\"" in line:
                self.errors["game_player_equip"].append(linenum + 1)

            if "\"classname\" \"trigger_hurt\"" in line:
                self.errors["trigger_hurt"].append(linenum + 1)

            if "\"classname\" \"env_spritetrail\"" in line:
                self.errors["env_spritetrail"].append(linenum + 1)

            if "\"classname\" \"logic_auto\"" in line:
                self.errors["logic_auto"].append(linenum + 1)

            if "\"classname\" \"logic_case\"" in line:
                self.errors["logic_case"].append(linenum + 1)

            if "\"classname\" \"logic_relay\"" in line:
                self.errors["logic_relay"].append(linenum + 1)

            if "\"classname\" \"logic_timer\"" in line:
                self.errors["logic_timer"].append(linenum + 1)

            if "\"classname\" \"env_shake\"" in line:
                self.errors["env_shake"].append(linenum + 1)

            if "\"classname\" \"func_rotating\"" in line:
                self.errors["func_rotating"].append(linenum + 1)

            if "\"classname\" \"func_button\"" in line:
                self.errors["func_button"].append(linenum + 1)

            if "\"classname\" \"func_rot_button\"" in line:
                self.errors["func_rot_button"].append(linenum + 1)

            if "\"classname\" \"point_servercommand\"" in line:
                self.errors["point_servercommand"].append(linenum + 1)

            if "\"classname\" \"func_breakable\"" in line:
                self.errors["func_breakable"].append(linenum + 1)

            if "\"classname\" \"env_hudhint\"" in line:
                self.errors["env_hudhint"].append(linenum + 1)

            if "\"classname\" \"game_text\"" in line:
                self.errors["game_text"].append(linenum + 1)

            if "\"classname\" \"func_door\"" in line:
                self.errors["func_door"].append(linenum + 1)

            if "\"classname\" \"func_movelinear\"" in line:
                self.errors["func_movelinear"].append(linenum + 1)

            if "\"classname\" \"player_speedmod\"" in line:
                self.errors["player_speedmod"].append(linenum + 1)

        return self.errors
