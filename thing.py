import bspsearch
import bspmodule
import os

REALLY_BAD_THINGS = ["jail", "weapon", "secret", "trigger_hurt", "env_shake",
                     "func_button", "func_rot_button", "env_hudhint", "game_text",
                     "player_speedmod"]


def get_surrounding_lines(lines, linenum):
    # Find the start of the block
    new_linenum = int(linenum)
    while lines[new_linenum] != "}":
        if lines[new_linenum] == "{":
            start_line = new_linenum
            break
        else:
            new_linenum -= 1
    # Start at the top and keep going until we reach the end
    new_linenum = start_line + 1
    while lines[new_linenum] != "{":
        if lines[new_linenum] == "}":
            end_line = new_linenum
            break
        else:
            new_linenum += 1
    start_and_end = (start_line, end_line)
    return start_and_end


path = os.path.abspath(
    r'E:\Desktop\maps_folder')
onlyfiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

with open("E:/Desktop/new_report.txt", "w+") as outputfile:
    for filenum, file in enumerate(onlyfiles):
        with open(os.path.join(path, file), "rb") as bsp:
            outputfile.write("%s:\n" % file)
            try:
                bspmod = bspmodule.BSP(bsp.read())
                entlump_raw = bspmod.get_lump(0)
                entlump_decoded = entlump_raw.decode(errors="ignore")
                bspsearch_inst = bspsearch.BSPsearch(entlump_decoded)
                errors = bspsearch_inst.search()
                for key, val in errors.items():
                    if val != []:
                        outputfile.write("    -%s:\n" % key)
                        for linenum in val:
                            start_and_end = get_surrounding_lines(bspsearch_inst.lines, linenum-1)
                            for line in range(start_and_end[0], start_and_end[1]):
                                outputfile.write(
                                    "        Line %d:  %s\n" % (int(line)+1,
                                                                bspsearch_inst.lines[line]))
            except bspmodule.BSPException:
                outputfile.write("BSPexception for file %s\n" % file)
        # print("%d/615 (%s)" % (filenum+1, file))
