# AutoStripperConfig #

Make config files for Bailopan's [Stripper:Source plugin](https://www.bailopan.net/stripper/) quickly and easily.


### Install ###

* You will need Python 3. [(Download it here)](https://www.python.org/getit/)
* [Download or clone AutoStripperConfig's files.](https://bitbucket.org/Bocuma747/autostripperconfig/src) You can delete README.md and .gitignore, but the rest of the files must be in the same directory as each other.
* Run main.py with Python 3.

Made by Bocuma.  

Contact me on Discord for any questions or bug reports:  
*Bocuma#2305*

License: GNU GPLv3