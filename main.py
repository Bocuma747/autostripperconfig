from tkinter import ttk, filedialog, scrolledtext, messagebox, DISABLED, sys
import tkinter as tk
import ctypes
import re
import bspmodule
import bspsearch

# Should fix blurry font when text scaling is enabled on Windows
if 'win' in tk.sys.platform:
    ctypes.windll.shcore.SetProcessDpiAwareness(1)

##########################################################################
# Big thanks to Bryan Oakley on StackOverflow for the line number bar... #
# ...and Bailopan for making Stripper open-source.                       #
##########################################################################


class Syntax:

    """Each block becomes a list whose 1st item is a string that tells
    the interpreter what kind of block it is ('add', 'filter' or 'modify').
    In 'add' and 'filter' blocks, the 2nd item in the list is a list of
    the block's properties (a.k.a. keyvalues).
    In 'modify' blocks, it will have one list per sub-mode
    ("match", "replace", "delete", and "insert", in that order).
    Each one of those lists will contain its corresponding keyvalues.
    For example, the following block:

    add:
    {
        "classname" "trigger_teleport"
        "origin" "123 456 789"
    }

    becomes ('add', ['"classname" "trigger_teleport"', '"origin" "123 456 789"']).

    And this block:

    modify:
    {
        match:
        {
        "origin" "1 2 3"
        }
        replace:
        {
        "target" "mapstart"
        }
    }

    becomes ['modify', [('match', ['"origin" "1 2 3"'])], [('replace', ['"target" "mapstart"'])]].
    """
    def __init__(self, textbox):
        mode = None
        submode = None
        in_block = False
        in_modify_block = False
        in_submode_block = False
        # prop_regex matches keyvalues in their proper format.
        prop_regex = re.compile("\".*\" \".*\"")
        props = []
        self.blocks = []
        list_insert = []
        list_delete = []
        list_match = []
        list_replace = []

        # Get every line in textbox, remove whitespace from all lines,
        # and add them to list_lines.
        list_lines = []
        for line in textbox.get("1.0", tk.END).splitlines():
            list_lines.append(line.strip())

        for linenum, line in enumerate(list_lines):
            # Ignore commented lines
            if line[:1] == ";" or line[:1] == "#" or line[:2] == "//":
                continue
            # Look for modes
            if line[:4] == "add:":
                mode = "add"
            elif line[:7] == "filter:" or line[:7] == "remove:":
                mode = "filter"
            elif line[:7] == "modify:":
                mode = "modify"
                submode = None
            # Look for sub-modes of Modify
            elif line[:6] == "match:" and mode == "modify":
                submode = "match"
            elif line[:8] == "replace:" and mode == "modify":
                submode = "replace"
            elif line[:7] == "delete:" and mode == "modify":
                submode = "delete"
            elif line[:7] == "insert:" and mode == "modify":
                submode = "insert"
            # If we find a new block and we aren't already in one,
            # set in_block to True and clear everything
            elif line == "{" and not in_block:
                in_block = True
                props = []
                list_insert = []
                list_delete = []
                list_match = []
                list_replace = []
                if mode == "modify":
                    in_modify_block = True
            # If we find the end of an Add or Filter block and we're already in one,
            # append them to the blocks list and set in_block to False.
            elif line == "}" and in_block and not in_modify_block:
                in_block = False
                if mode == "filter":
                    self.blocks.append(("filter", props))
                elif mode == "add":
                    self.blocks.append(("add", props))

            # If we find the end of a Modify block and we are already in one,
            # add its sub-blocks to the main block, then add the block to the block list.
            # Also reset in_(modify_)block back to False.
            elif (line == "}"
                  and in_block
                  and in_modify_block
                  and not in_submode_block):
                in_block = False
                in_modify_block = False
                # Create a list specifically for this Modify block
                modify_block = ["modify"]
                # Iterate through each sub-mode list
                for submode_list in [list_match,
                                     list_replace,
                                     list_delete,
                                     list_insert]:
                    # Ignore empty sub-mode lists
                    if submode_list == []:
                        continue
                    # Add sub-mode lists to this Modify block
                    else:
                        modify_block.append(submode_list[0])
                # Append this Modify block to the blocks list
                self.blocks.append(modify_block)
            # If we find the end of a sub-block,
            # flush all submode properties to their respective lists
            elif (line == "}"
                  and in_block
                  and in_modify_block
                  and in_submode_block):
                in_submode_block = False
                if mode == "modify":
                    if submode is None:
                        props = []
                        continue
                    elif submode == "insert":
                        list_insert.append(("insert", props))
                        props = []
                        continue
                    elif submode == "delete":
                        list_delete.append(("delete", props))
                        props = []
                        continue
                    elif submode == "match":
                        list_match.append(("match", props))
                        props = []
                        continue
                    elif submode == "replace":
                        list_replace.append(("replace", props))
                        props = []
                        continue

            # If we find the start of a sub-block, set in_submode_block to True.
            elif (line == "{"
                  and in_block
                  and in_modify_block
                  and submode is not None):
                in_submode_block = True

            # If we're in an Add or Filter block, or a sub-block,
            # and we reach a line that isn't a bracket, then it's a prop.
            # Check if it's valid with regex and add it to props list.
            elif ((in_block and not in_modify_block)
                  or (in_block and in_modify_block and in_submode_block)):
                if prop_regex.match(line) is not None:
                    props.append(line)
                else:
                    props.append("<invalid keyvalue at line %d>" % (linenum+1))


class TextLineNumbers(tk.Canvas):
    def __init__(self, *args, **kwargs):
        tk.Canvas.__init__(self, *args, **kwargs)
        self.textwidget = None

    def attach(self, text_widget):
        self.textwidget = text_widget

    def redraw(self, *args):
        '''redraw line numbers'''
        self.delete("all")

        i = self.textwidget.index("@0,0")
        while True:
            dline = self.textwidget.dlineinfo(i)
            if dline is None:
                break
            y = dline[1]
            linenum = str(i).split(".")[0]
            self.create_text(2, y, anchor="nw", text=linenum,
                             font=("Courier", "10"), fill="gray")
            i = self.textwidget.index("%s+1line" % i)


class MainGUI:

    # Apparently every tk widget has a different way of selecting text.
    # Can't believe I need 3 functions for this...
    def selectall_entry(self, event):
        event.widget.select_range(0, "end")

    def selectall_text(self, event):
        event.widget.tag_add("sel", "1.0", "end")

    def selectall_spinbox(self, event):
        event.widget.selection_clear()
        event.widget.focus_set()
        event.widget.selection("range", "0", "end")

    # Redo is bound to ctrl-shift-Z instead of ctrl-Y on Linux, so we have to manually bind it.
    def redo(self, event):
        event.widget.event_generate("<<Redo>>")

    def on_scrollbar(self, *args):
        self.textbox.yview(*args)
        self.linenumbers.redraw()

    def on_text_scroll(self, *args):
        self.scrollbar_y.set(*args)
        self.on_scrollbar("moveto", args[0])

    def on_textbox_update(self, event):
        modified = self.textbox.edit_modified()
        if modified:
            # Add an asterisk to the title when there are unsaved changes
            if self.lastsavecache != self.textbox.get("1.0", "end-1c"):
                self.master.title("%s * -- AutoStripperConfig" % self.docname)
            else:
                self.master.title("%s -- AutoStripperConfig" % self.docname)
            # Update search when textbox is modified
            if self.searchbox_exists:
                self.searchbox.findall()
            # Clear tree
            self.tree.delete(*self.tree.get_children())
            # Clear red lines in textbox
            self.textbox.tag_remove("red", "0.0", "end")
            self.linenumbers.redraw()

            syntaxcheck = Syntax(self.textbox)
            blockslist = syntaxcheck.blocks
            for block in blockslist:
                if block[0] == "add" or block[0] == "filter":
                    parent = self.tree.insert('', 'end',
                                              text=block[0], open=True)
                    for prop in block[1]:
                        if prop[:25] != "<invalid keyvalue at line":
                            self.tree.insert(parent, 'end',
                                             text=prop, open=True)
                        else:
                            self.tree.insert(parent, 'end',
                                             text=prop, open=True,
                                             tag="red")
                            self.textbox.tag_add(
                                "red",
                                "%d.0" % int(prop[26:-1]),
                                "%d.end" % int(prop[26:-1])
                            )
                elif block[0] == "modify":
                    parent = self.tree.insert('', 'end',
                                              text=block[0], open=True)
                    for submode in block[1:]:
                        child = self.tree.insert(parent, 'end',
                                                 text=submode[0], open=True)
                        for prop in submode[1]:
                            if prop[:25] != "<invalid keyvalue at line":
                                self.tree.insert(child, 'end',
                                                 text=prop, open=True)
                            else:
                                self.tree.insert(child, 'end',
                                                 text=prop, open=True,
                                                 tag="red")
                                self.textbox.tag_add(
                                    "red",
                                    "%d.0" % int(prop[26:-1]),
                                    "%d.end" % int(prop[26:-1])
                                )
        # Reset <<Modified>> flag so it can be detected again
        self.textbox.edit_modified(False)

    def on_exit(self):
        # If textbox has been modified since the last save, ask user before exiting.
        if self.lastsavecache != self.textbox.get("1.0", "end-1c"):
            askquit = tk.messagebox.askquestion(
                "",
                "You have unsaved changes. Exit anyway?",
                parent=self.master
                )
            if askquit == "yes":
                self.master.destroy()
        # If it hasn't been changed, quit without asking.
        else:
            self.master.destroy()

    def on_load(self, file):
        if file is None:
            self.file = None
            self.docname = "<New document>"
            self.lastsavecache = ""
            self.textbox.delete("1.0", "end-1c")
        else:
            self.file = file
            self.docname = file.name
            self.lastsavecache = file.read()
            self.textbox.delete("1.0", "end-1c")
            self.textbox.insert(tk.END, self.lastsavecache)

    def on_searchbox_exit(self, event):
        self.searchbox_exists = False

    def __init__(self, master, file):
        self.master = master
        self.file = file
        self.searchbox_exists = False
        self.docname = "<New document>"
        self.lastsavecache = ""

        master.title("%s -- AutoStripperConfig" % self.docname)

        master.bind("<Control-o>", lambda event: self.ctrl_o("break"))
        master.bind("<Control-n>", self.ctrl_n)
        master.bind("<Control-s>", self.ctrl_s)
        master.bind("<Control-Shift-KeyPress-S>", self.ctrl_shift_s)
        master.bind("<Control-f>", self.ctrl_f)
        master.bind("<<SearchBoxExit>>", self.on_searchbox_exit)
        master.bind_class("TEntry", "<Control-a>", self.selectall_entry)
        master.bind_class("TEntry", "<Control-y>", self.redo)
        master.bind_class("Spinbox", "<Control-a>", self.selectall_spinbox)
        master.bind_class("Text", "<Control-a>", self.selectall_text)
        master.bind_class("Text", "<Control-y>", self.redo)

        self.menubar = tk.Menu(master)
        master.config(menu=self.menubar)
        # FILE
        self.menubar_file = tk.Menu(self.menubar, tearoff=False)
        self.menubar_file.add_command(label="New                Ctrl+N",
                                      command=self.ctrl_n)
        self.menubar_file.add_command(label="Open               Ctrl+O",
                                      command=lambda event=None: self.ctrl_o(event))
        self.menubar_file.add_command(label="Save                Ctrl+S",
                                      command=self.ctrl_s,
                                      state=tk.DISABLED)
        self.menubar_file.add_command(label="Save As...        Ctrl+Shift+S",
                                      command=self.ctrl_shift_s)
        self.menubar_file.add_separator()
        self.menubar_file.add_command(label="Exit", command=self.on_exit)
        self.menubar.add_cascade(label="File", menu=self.menubar_file)
        # VIEW
        self.menubar_view = tk.Menu(self.menubar, tearoff=False)
        self.menubar_view.add_command(label="Find and Replace       Ctrl+F",
                                      command=self.ctrl_f)
        self.menubar_view.add_command(label="View .BSP entity lump...",
                                      command=self.launch_window_lumpviewer)
        self.menubar.add_cascade(label="View", menu=self.menubar_view)

        tk.Grid.rowconfigure(master, 0, weight=1, minsize=64)
        tk.Grid.columnconfigure(master, 0, weight=1, minsize=64)

        self.pane = tk.PanedWindow(orient=tk.HORIZONTAL,
                                   relief=tk.FLAT,
                                   sashrelief=tk.SOLID,
                                   sashwidth=2,
                                   sashpad=8,
                                   bg="#f0f0f0",
                                   opaqueresize=1)
        self.pane.grid(row=0, column=0, sticky="nsew")

        # The left part of the window
        self.side1 = tk.Frame(self.pane)
        self.side1.grid(row=0, column=0, sticky="nsew")
        tk.Grid.rowconfigure(self.side1, 1, weight=1)
        tk.Grid.columnconfigure(self.side1, 0, weight=1)

        # The right part of the window
        self.side2 = tk.Frame(self.pane)
        self.side2.grid(row=0, column=1, sticky="nsew")
        tk.Grid.rowconfigure(self.side2, 0, weight=1)
        tk.Grid.rowconfigure(self.side2, 1, weight=1)
        tk.Grid.columnconfigure(self.side2, 2, weight=1)

        # side1's contents
        self.frame = ttk.Frame(self.side1, borderwidth=1, relief=tk.SOLID)
        self.frame.grid(row=0, column=0,
                        sticky=tk.N+tk.E+tk.W, padx=10, pady=20)
        tk.Grid.columnconfigure(self.frame, 0, weight=1)
        self.button_addent = ttk.Button(self.frame,
                                        text="Add an entity...",
                                        command=self.launch_window_addent)
        self.button_addent.grid(row=0, column=0,
                                sticky=tk.NSEW, padx=20, pady=20)
        self.button_filterent = ttk.Button(self.frame,
                                           text="Filter an entity...",
                                           command=self.launch_window_filterent)
        self.button_filterent.grid(row=1, column=0,
                                   sticky=tk.NSEW, padx=20, pady=20)
        self.button_modent = ttk.Button(self.frame,
                                        text="Modify an entity...",
                                        command=self.launch_window_modent)
        self.button_modent.grid(row=2, column=0,
                                sticky=tk.NSEW, padx=20, pady=20)

        # side2's contents
        self.textbox = tk.Text(self.side2, padx=6, borderwidth=0, wrap=tk.NONE,
                               undo=True, maxundo=-1, autoseparators=True)
        self.textbox.grid(row=0, column=2, rowspan=2,
                          sticky="nsew", pady=(20, 0))
        self.textbox.bind("<<Modified>>", self.on_textbox_update)
        self.textbox.bind("<Control-o>", lambda event: self.ctrl_o("break"))
        self.textbox.tag_configure("red", background="#f25c66")

        self.linenumbers = TextLineNumbers(self.side2, width=30,
                                           borderwidth=0, takefocus=0)
        self.linenumbers.attach(self.textbox)
        self.linenumbers.grid(row=0, column=1, rowspan=2,
                              sticky="nsew", pady=(20, 0))

        self.scrollbar_x = ttk.Scrollbar(self.side2, orient=tk.HORIZONTAL,
                                         command=self.textbox.xview)
        self.scrollbar_x.grid(row=2, column=2, columnspan=1, sticky=tk.NSEW)
        self.scrollbar_y = ttk.Scrollbar(self.side2, command=self.on_scrollbar)
        self.scrollbar_y.grid(row=0, column=3, rowspan=2,
                              sticky=tk.NSEW, padx=(0, 5), pady=(20, 0))
        self.textbox.configure(xscrollcommand=self.scrollbar_x.set,
                               yscrollcommand=self.on_text_scroll)
        self.linenumbers.configure(yscrollcommand=self.on_text_scroll)

        self.tree = ttk.Treeview(self.side1, show="tree")
        self.tree.tag_configure("red", background="#f25c66")
        self.tree.grid(row=1, column=0, rowspan=1,
                       sticky="nsew", padx=10, pady=15)

        self.pane.add(self.side1)
        self.pane.add(self.side2)

        # Window geometry
        master.update()
        self.screenwidth = master.winfo_screenwidth()
        self.screenheight = master.winfo_screenheight()
        self.initwindowsize = "%dx%d" % (self.screenwidth, self.screenheight)
        master.geometry(self.initwindowsize)
        # Call on_exit if user quits via the window manager
        master.protocol("WM_DELETE_WINDOW", self.on_exit)

    def togglesave(self):
        if self.file:
            self.menubar_file.entryconfig(0, state=tk.NORMAL)
        else:
            self.menubar_file.entryconfig(0, state=tk.DISABLED)

    def ctrl_n(self, event=None):
        if self.lastsavecache != self.textbox.get("1.0", "end-1c"):
            askquit = tk.messagebox.askquestion(
                "",
                "You have unsaved changes. Exit this document anyway?",
                parent=self.master
                )
            if askquit == "yes":
                self.file = None
                self.on_load(self.file)
        else:
            self.file = None
            self.on_load(self.file)

    def ctrl_o(self, event):
        self.stripperfilepath = filedialog.askopenfilename(
            title="Open Stripper:Source config file",
            filetypes=[("Stripper:Source Config (.cfg)",
                        "*.cfg")]
            )
        if self.stripperfilepath:
            with open(self.stripperfilepath, "r") as stripperfile:
                if self.lastsavecache != self.textbox.get("1.0", "end-1c"):
                    askquit = tk.messagebox.askquestion(
                        "",
                        "You have unsaved changes. Exit this document anyway?",
                        parent=self.master
                        )
                    if askquit == "yes":
                        self.on_load(stripperfile)
                else:
                    self.on_load(stripperfile)
        return "break"

    def ctrl_s(self, event=None):
        if self.file is None:
            self.savefile = filedialog.asksaveasfile(
                mode="w",
                defaultextension=".cfg",
                title="Save config as...",
                filetypes=[("Stripper:Source Config (.cfg)", "*.cfg")])
            if self.savefile:
                self.file = self.savefile
                self.file.write(self.textbox.get("1.0", "end-1c"))
                self.file.close()
                self.lastsavecache = self.textbox.get("1.0", "end-1c")
                self.docname = self.savefile.name
                self.master.title("%s -- AutoStripperConfig" % self.docname)
        else:
            with open(self.file.name, "w") as self.openedfile:
                self.openedfile.write(self.textbox.get("1.0", "end-1c"))
                self.lastsavecache = self.textbox.get("1.0", "end-1c")
                self.docname = self.file.name
                self.master.title("%s -- AutoStripperConfig" % self.docname)
        self.togglesave()

    def ctrl_shift_s(self, event=None):
        self.savefile = filedialog.asksaveasfile(
            mode="w",
            defaultextension=".cfg",
            title="Save config as...",
            filetypes=[("Stripper:Source Config (.cfg)", "*.cfg")])
        if self.savefile:
            self.file = self.savefile
            self.file.write(self.textbox.get("1.0", "end-1c"))
            self.file.close()
            self.lastsavecache = self.textbox.get("1.0", "end-1c")
            self.docname = self.file.name
            self.master.title("%s -- AutoStripperConfig" % self.docname)
        self.togglesave()

    def ctrl_f(self, event=None):
        # Only make a new SearchBox is there isn't already one
        if not self.searchbox_exists:
            self.searchbox_exists = True
            self.searchbox = SearchBox(self.side2, self.textbox)
            self.searchbox.grid(row=2, column=1, columnspan=3,
                                sticky="nsew", pady=(0, 15))
            self.master.bind("<Escape>", lambda x: exit_searchbox())

        def exit_searchbox():
            self.searchbox_exists = False
            self.searchbox.exit()

    def launch_window_addent(self):
        self.new_toplevel = tk.Toplevel(self.master)
        self.window_addent = EntGUI(
            self.new_toplevel,
            "Add entity",
            "Add an entity with the following properties:",
            "add:\n",
            self.textbox
            )

    def launch_window_filterent(self):
        self.new_toplevel = tk.Toplevel(self.master)
        self.window_filterent = EntGUI(
            self.new_toplevel,
            "Filter entities",
            "Filter out any entities with the following properties:",
            "filter:\n",
            self.textbox
            )

    def launch_window_modent(self):
        self.new_toplevel = tk.Toplevel(self.master)
        self.window_modent = ModEntGUI(self.new_toplevel, self.textbox)

    def launch_window_lumpviewer(self):
        self.new_toplevel = tk.Toplevel(self.master)
        self.window_lumpviewer = LumpViewer(self.new_toplevel)


class EntGUI:

    def ctrl_o_break(self, event):
        """Fixes ctrl-o adding a newline"""
        return "break"

    def on_scrollbar(self, *args):
        self.entgui_textbox.yview(*args)
        self.linenumbers.redraw()

    def on_text_scroll(self, *args):
        self.scrollbar_y.set(*args)
        self.on_scrollbar("moveto", args[0])

    # Called when user types in either entry box
    def on_type(self, event):
        # If either entry is empty, disable the Insert button.
        if self.entry_key.get() == "":
            self.button_insert.state(["disabled"])
        elif self.entry_value.get() == "":
            self.button_insert.state(["disabled"])
        # If both have been typed in, enable button_insert.
        else:
            self.button_insert.state(["!disabled"])

    # Called when textbox is changed, either by user or the program
    def on_textbox_update(self, event):
        modified = self.entgui_textbox.edit_modified()
        if modified:
            self.linenumbers.redraw()
            if self.linecount != self.entgui_textbox.index("end - 1 line").split('.')[0]:
                self.linecount = self.entgui_textbox.index("end - 1 line").split('.')[0]
                if int(self.linecount) > 1:
                    self.spinbox_LineNum.config(to=self.linecount)
        # Reset the edit_modified flag so it'll work again
        self.entgui_textbox.edit_modified(False)

    # Called by button_insert;
    # formats and inserts the contents of the entry boxes into the textbox.
    def insert_keyvalue(self):
        BadInput = False
        # If the user chose to add the line to the end:
        if self.radioVar.get() == "end":
            # Set insert_location to the line before the }
            self.insert_location = self.entgui_textbox.index('end - 1 line')

            # Insert line into textbox now that its location has been set.
            self.entgui_textbox.insert(
                self.insert_location,
                "    \"%s\" \"%s\"\n" % (self.entry_key.get(),
                                         self.entry_value.get())
                )

            # Clear the entry boxes and disable the Insert button
            self.entry_key.delete(0, tk.END)
            self.entry_value.delete(0, tk.END)
            self.button_insert.state(["disabled"])

        # If the user chose a custom line to insert it at:
        elif self.radioVar.get() == "line":
            # Make sure the user input an integer
            try:
                int(self.spinbox_LineNum.get())
            except ValueError:
                tk.messagebox.showerror("Error",
                                        "Please only insert integers.",
                                        parent=self.master)
                BadInput = True

            # If the input is ok:
            if not BadInput:
                # Set insert_location to the spinbox's value.
                self.insert_location = "%s.0" % self.spinbox_LineNum.get()

                # Insert line into textbox now that its location has been set.
                self.entgui_textbox.insert(
                    self.insert_location,
                    "    \"%s\" \"%s\"\n" % (self.entry_key.get(),
                                             self.entry_value.get())
                    )

                # Clear the entry boxes and disable the Insert button
                self.entry_key.delete(0, tk.END)
                self.entry_value.delete(0, tk.END)
                self.button_insert.state(["disabled"])

    # Called when either of the two radiobuttons are selected
    def toggle_spinbox(self):
        # Disable the spinbox if End is selected
        if self.radioVar.get() == "end":
            self.spinbox_LineNum.config(state="disabled")
        # Enable the spinbox if Line is selected
        elif self.radioVar.get() == "line":
            self.spinbox_LineNum.config(state="normal")

    def __init__(self, master, title, titletext, token, textbox):
        self.textbox = textbox
        self.master = master
        master.title(title)

        tk.Grid.rowconfigure(master, 2, weight=1)
        tk.Grid.columnconfigure(master, 1, weight=1, minsize=60)

        # Because on_textbox_update gets called when the user opens the window,
        # we need to set these vars manually first.
        # Otherwise on_textbox_update will throw an error.
        self.linecount = 4
        self.insert_location = "4.0"

        self.entgui_textbox = tk.Text(master, padx=6,
                                      borderwidth=0, wrap=tk.NONE,
                                      undo=True, maxundo=-1,
                                      autoseparators=True)
        self.entgui_textbox.grid(row=1, column=1, rowspan=2,
                                 sticky=tk.NSEW, padx=(10, 0))
        # Call on_textbox_update whenever textbox gets modified
        self.entgui_textbox.bind("<<Modified>>", self.on_textbox_update)
        self.entgui_textbox.bind("<Control-o>", self.ctrl_o_break)
        # token will be either "add:" or "filter:"
        self.entgui_textbox.insert(1.0, token)
        # add a {
        self.entgui_textbox.insert(2.0, "{\n")
        # ...and finish it off with a }
        self.entgui_textbox.insert(3.0, "}")

        self.linenumbers = TextLineNumbers(master, width=30,
                                           borderwidth=0, takefocus=0)
        self.linenumbers.attach(self.entgui_textbox)
        self.linenumbers.grid(row=1, column=0, rowspan=2,
                              sticky=tk.NSEW, padx=(10, 0))

        self.scrollbar_x = ttk.Scrollbar(master, orient=tk.HORIZONTAL,
                                         command=self.entgui_textbox.xview)
        self.scrollbar_x.grid(row=3, column=1, columnspan=1,
                              sticky=tk.NSEW, padx=(10, 0))

        self.scrollbar_y = ttk.Scrollbar(master,
                                         command=self.on_scrollbar)
        self.scrollbar_y.grid(row=1, column=2, rowspan=2,
                              sticky=tk.NSEW, padx=(0, 5))

        self.entgui_textbox.configure(xscrollcommand=self.scrollbar_x.set,
                                      yscrollcommand=self.on_text_scroll)
        self.linenumbers.configure(yscrollcommand=self.on_text_scroll)

        self.label1 = ttk.Label(master, text=titletext)
        self.label1.grid(row=0, column=0, columnspan=4,
                         sticky=tk.NSEW, padx=10, pady=20)

        self.frame1 = ttk.LabelFrame(master, text="Insert Keyvalue")
        self.frame1.grid(row=1, column=3, sticky=tk.NSEW, padx=10, pady=10)

        ttk.Label(self.frame1, text="Key").grid(
            row=0, column=0, sticky=tk.NSEW,
            padx=10, pady=(10, 0)
            )

        ttk.Label(self.frame1, text="Value").grid(
            row=0, column=1, sticky=tk.NSEW,
            padx=10, pady=(10, 0)
            )

        # Where the user types the key
        self.entry_key = ttk.Entry(self.frame1, width=20)
        self.entry_key.bind("<KeyRelease>", lambda event: self.on_type(event))
        self.entry_key.grid(row=1, column=0,
                            sticky=tk.N+tk.E+tk.W, padx=10, pady=10)

        # Where the user types the value
        self.entry_value = ttk.Entry(self.frame1, width=20)
        self.entry_value.bind("<KeyRelease>",
                              lambda event: self.on_type(event))
        self.entry_value.grid(row=1, column=1, columnspan=2,
                              sticky=tk.N+tk.E+tk.W, padx=10, pady=10)

        ttk.Label(self.frame1, text="Insert At:").grid(
            row=2, column=0, sticky=tk.NSEW,
            padx=10, pady=(10, 0)
            )

        # self.radioVar corresponds to the two radiobuttons
        # and will always equal either "end" or "line".
        self.radioVar = tk.StringVar()
        self.radioVar.set("end")    # Set default value to "end"

        # If this button ("End") is selected, new lines from the entry boxes
        # will be inserted at the bottom line of the textbox.
        self.radioEnd = ttk.Radiobutton(self.frame1, text="End",
                                        variable=self.radioVar,
                                        value="end",
                                        command=self.toggle_spinbox)
        self.radioEnd.state(['selected'])   # Select this button by default
        self.radioEnd.grid(row=3, column=0,
                           sticky=tk.W, padx=10, pady=(10, 0))

        # If this button ("Line #:") is selected, it enables spinbox_LineNum,
        # where the user can input a custom line number to insert new lines at.
        self.radioLineNum = ttk.Radiobutton(self.frame1, text="Line #:",
                                            variable=self.radioVar,
                                            value="line",
                                            command=self.toggle_spinbox)
        self.radioLineNum.grid(row=4, column=0,
                               sticky=tk.W, padx=10, pady=(10, 0))

        # Speaking of that spinbox, let's make it:
        self.spinbox_LineNum = tk.Spinbox(self.frame1,
                                          from_=1, to=self.linecount,
                                          increment=1, width=5)
        # Clear the spinbox
        self.spinbox_LineNum.delete(0, "end")
        # Set default value to 3
        self.spinbox_LineNum.insert(0, 3)
        # Disabled until user selects self.radioLineNum button
        self.spinbox_LineNum.config(state="disabled")
        self.spinbox_LineNum.grid(row=4, column=0,
                                  sticky=tk.E, padx=0, pady=(10, 0))

        # Disabled until user has typed something into both entry boxes
        self.button_insert = ttk.Button(self.frame1, text="Insert Keyvalue",
                                        command=self.insert_keyvalue,
                                        state="disabled")
        self.button_insert.grid(row=5, column=0, columnspan=3,
                                sticky=tk.NSEW, padx=10, pady=10)

        self.separator = ttk.Separator(master, orient=tk.HORIZONTAL)
        self.separator.grid(row=4, column=0, columnspan=4,
                            sticky=tk.EW, pady=20)

        self.buttonOK = ttk.Button(master, text="OK",
                                   command=self.save_ent)
        self.buttonOK.grid(row=5, column=0, columnspan=4, pady=(0, 20))

        self.buttonCancel = ttk.Button(master, text="Cancel",
                                       command=master.destroy)
        self.buttonCancel.grid(row=5, column=3,
                               sticky=tk.E, padx=(0, 40), pady=(0, 20))

        # Window geometry
        master.update()
        self.screenwidth = master.winfo_screenwidth()
        self.screenheight = master.winfo_screenheight()
        master.geometry("%dx%d" % (self.screenwidth/2.5,
                                   self.screenheight/1.5))

    # Inserts everything from EntGUI's textbox into MainGUI's textbox.
    def save_ent(self):
        self.textbox.insert(self.textbox.index('end - 1 line'),
                            self.entgui_textbox.get("1.0", tk.END))
        self.master.destroy()


class ModEntGUI:

    def ctrl_o_break(self, event):
        """Fixes ctrl-o adding a newline"""
        return "break"

    def on_scrollbar(self, *args):
        self.modentgui_textbox.yview(*args)
        self.linenumbers.redraw()

    def on_text_scroll(self, *args):
        self.scrollbar_y.set(*args)
        self.on_scrollbar("moveto", args[0])

    # Called when user types in either entry box
    def on_type(self, event):
        # If either entry is empty, disable button_insert_kv.
        if self.entry_key.get() == "":
            self.button_insert_kv.state(["disabled"])
        elif self.entry_value.get() == "":
            self.button_insert_kv.state(["disabled"])
        # If both have been typed in, enable button_insert_kv.
        else:
            self.button_insert_kv.state(["!disabled"])

    # Called when textbox is changed, either by user or the program.
    def on_textbox_update(self, event):
        # Refresh the line number bar
        self.linenumbers.redraw()

        modified = self.modentgui_textbox.edit_modified()
        if modified:
            # If the number of lines in the textbox has changed:
            if self.linecount != self.modentgui_textbox.index("end - 1 line").split('.')[0]:
                # Update linecount
                self.linecount = self.modentgui_textbox.index("end - 1 line").split('.')[0]
                # Update the max value of each spinbox
                if int(self.linecount) > 1:
                    self.spin_line_Action.config(to=self.linecount)
                    self.spin_line_kv.config(to=self.linecount)

        # Reset the edit_modified flag so it'll work again
        self.modentgui_textbox.edit_modified(False)

    # Called by button_insert_Action; inserts the selected action into the textbox.
    def insert_action(self):
        BadInput = False
        # If the user chose to add the line to the end:
        if self.var_line_Action.get() == "end":
            # Set insert_location_Action to the line before the }
            self.insert_location_Action = self.modentgui_textbox.index("end - 1 line")
            # Insert it now that we have our location.
            self.modentgui_textbox.insert(
                self.insert_location_Action,
                "    %s\n    {\n    }\n" % self.var_Action.get()
                )
        # If the user chose a custom line to insert it at:
        elif self.var_line_Action.get() == "line":
            # Make sure the user input an integer
            try:
                int(self.spin_line_Action.get())
            except ValueError:
                tk.messagebox.showerror("Error",
                                        "Please only insert integers.",
                                        parent=self.master)
                BadInput = True
            # If the input is ok:
            if not BadInput:
                # Set insert_location_Action to the spinbox's value.
                self.insert_location_Action = "%s.0" % self.spin_line_Action.get()
                # Insert it now that we have our location.
                self.modentgui_textbox.insert(
                    self.line_Action,
                    "    %s\n    {\n    }\n" % self.var_Action.get()
                    )

    # Called by button_insert_kv;
    # formats and inserts the contents of the entry boxes into the textbox.
    def insert_keyvalue(self):
        BadInput = False
        # If the user chose to add the line to the end:
        if self.var_line_kv.get() == "end":
            # Set line_kv to the line before the }
            self.line_kv = self.modentgui_textbox.index("end - 1 line")
            # Insert keyvalue into textbox now that its location has been set.
            self.modentgui_textbox.insert(
                self.line_kv,
                "    \"%s\" \"%s\"\n" % (self.entry_key.get(),
                                         self.entry_value.get())
                )
            # Clear the entry boxes and disable button_insert_kv
            self.entry_key.delete(0, tk.END)
            self.entry_value.delete(0, tk.END)
            self.button_insert_kv.state(["disabled"])

        # If the user chose a custom line to insert it at:
        elif self.var_line_kv.get() == "line":
            # Make sure the user input an integer
            try:
                int(self.spin_line_kv.get())
            except ValueError:
                tk.messagebox.showerror("Error",
                                        "Please only insert integers.",
                                        parent=self.master)
                BadInput = True
            # If the input is ok:
            if not BadInput:
                # Set line_kv to the spinbox's value.
                self.line_kv = "%s.0" % self.spin_line_kv.get()
                # Insert keyvalue into textbox now that its location has been set.
                self.modentgui_textbox.insert(
                    self.line_kv,
                    "    \"%s\" \"%s\"\n" % (self.entry_key.get(),
                                             self.entry_value.get())
                    )
                # Clear the entry boxes and disable button_insert_kv
                self.entry_key.delete(0, tk.END)
                self.entry_value.delete(0, tk.END)
                self.button_insert_kv.state(["disabled"])

    # Called when either of the two radiobuttons in the Action section are selected.
    def toggle_spin_Action(self):
        # Disable the spinbox if End is selected
        if self.var_line_Action.get() == "end":
            self.spin_line_Action.config(state="disabled")
        # Enable the spinbox if Line is selected
        elif self.var_line_Action.get() == "line":
            self.spin_line_Action.config(state="normal")

    # Called when either of the two radiobuttons in the Keyvalue section are selected.
    def toggle_spin_kv(self):
        # Disable the spinbox if End is selected
        if self.var_line_kv.get() == "end":
            self.spin_line_kv.config(state="disabled")
        # Enable the spinbox if Line is selected
        elif self.var_line_kv.get() == "line":
            self.spin_line_kv.config(state="normal")

    # Inserts everything from ModEntGUI's textbox into MainGUI's textbox.
    def save_ent(self):
        self.textbox.insert(self.textbox.index("end - 1 line"),
                            self.modentgui_textbox.get("1.0", tk.END))
        self.master.destroy()

    def __init__(self, master, textbox):
        self.master = master
        self.textbox = textbox
        master.title("Modify an entity")

        tk.Grid.rowconfigure(master, 3, weight=1)
        tk.Grid.columnconfigure(master, 0, weight=0)
        tk.Grid.columnconfigure(master, 1, weight=1, minsize=60)

        # Because on_textbox_update gets called the moment the user
        # opens the window, we need to set these manually first.
        # Otherwise on_textbox_update won't have any values to work with and it'll throw an error.
        self.linecount = 4
        self.line_Action = "4.0"
        self.line_kv = "4.0"

        self.modentgui_textbox = tk.Text(master, padx=6,
                                         borderwidth=0, wrap=tk.NONE,
                                         undo=True, maxundo=-1,
                                         autoseparators=True)
        self.modentgui_textbox.grid(row=1, column=1, rowspan=3,
                                    sticky=tk.NSEW, padx=(10, 0))
        # Call on_textbox_update whenever textbox gets modified
        self.modentgui_textbox.bind("<<Modified>>", self.on_textbox_update)
        self.modentgui_textbox.bind("<Control-o>", self.ctrl_o_break)
        # Start with the "modify:" token
        self.modentgui_textbox.insert(1.0, "modify:\n")
        # add a {
        self.modentgui_textbox.insert(2.0, "{\n")
        # and finish it off with a }
        self.modentgui_textbox.insert(3.0, "}")

        self.linenumbers = TextLineNumbers(master, width=30,
                                           borderwidth=0, takefocus=0)
        self.linenumbers.attach(self.modentgui_textbox)
        self.linenumbers.grid(row=1, column=0, rowspan=3,
                              sticky=tk.NSEW, padx=(10, 0))

        self.scrollbar_x = ttk.Scrollbar(master, orient=tk.HORIZONTAL,
                                         command=self.modentgui_textbox.xview)
        self.scrollbar_x.grid(row=4, column=1, columnspan=1,
                              sticky=tk.S+tk.E+tk.W, padx=(10, 0))
        self.scrollbar_y = ttk.Scrollbar(master, command=self.on_scrollbar)
        self.scrollbar_y.grid(row=1, column=2, rowspan=3, sticky=tk.NSEW)
        self.modentgui_textbox.configure(xscrollcommand=self.scrollbar_x.set,
                                         yscrollcommand=self.on_text_scroll)
        self.linenumbers.configure(yscrollcommand=self.on_text_scroll)

        self.label1 = ttk.Label(
            master,
            text="Match entities and modify their keyvalues."
            )
        self.label1.grid(row=0, column=0, columnspan=4,
                         sticky=tk.NSEW, padx=10, pady=20)

        # This frame contains the Action section of the UI
        self.frame1 = ttk.LabelFrame(master, text="Insert Action")
        self.frame1.grid(row=1, column=3,
                         sticky=tk.NSEW, padx=10, pady=10)

        self.L2 = ttk.Label(self.frame1, text="Action:")
        self.L2.grid(row=0, column=0, columnspan=5,
                     sticky=tk.W, padx=10, pady=(10, 0))

        # Corresponds to the "match", "replace", "insert" and "delete" buttons.
        self.var_Action = tk.StringVar()
        self.var_Action.set("match:")

        # "match:"
        self.radio_action_Match = ttk.Radiobutton(self.frame1,
                                                  text="match:",
                                                  variable=self.var_Action,
                                                  value="match:")
        # Select this button by default
        self.radio_action_Match.state(["selected"])
        self.radio_action_Match.grid(row=1, column=0,
                                     sticky=tk.W, padx=10, pady=10)

        # "replace:"
        self.radio_action_Replace = ttk.Radiobutton(self.frame1,
                                                    text="replace:",
                                                    variable=self.var_Action,
                                                    value="replace:")
        self.radio_action_Replace.grid(row=1, column=1,
                                       sticky=tk.W, padx=10, pady=10)

        # "insert:"
        self.radio_action_Insert = ttk.Radiobutton(self.frame1,
                                                   text="insert:",
                                                   variable=self.var_Action,
                                                   value="insert:")
        self.radio_action_Insert.grid(row=1, column=2,
                                      sticky=tk.W, padx=10, pady=10)

        # "delete:"
        self.radio_action_Delete = ttk.Radiobutton(self.frame1,
                                                   text="delete:",
                                                   variable=self.var_Action,
                                                   value="delete:")
        self.radio_action_Delete.grid(row=1, column=3,
                                      sticky=tk.W, padx=10, pady=10)

        self.L3 = ttk.Label(self.frame1, text="Insert At:")
        self.L3.grid(row=2, column=0, sticky=tk.NSEW, padx=10, pady=(10, 0))

        # Corresponds to the two "Insert At:" buttons in the Action section.
        # Will always equal either "end" or "line".
        self.var_line_Action = tk.StringVar()
        # Set default value to "end"
        self.var_line_Action.set("end")

        self.radio_action_End = ttk.Radiobutton(self.frame1,
                                                text="End",
                                                variable=self.var_line_Action,
                                                value="end",
                                                command=self.toggle_spin_Action)
        # Select this button by default
        self.radio_action_End.state(["selected"])
        self.radio_action_End.grid(row=3, column=0,
                                   sticky=tk.W, padx=10, pady=(10, 0))

        self.radio_action_line = ttk.Radiobutton(self.frame1,
                                                 text="Line #:",
                                                 variable=self.var_line_Action,
                                                 value="line",
                                                 command=self.toggle_spin_Action)
        self.radio_action_line.grid(row=4, column=0,
                                    sticky=tk.W, padx=10, pady=(10, 0))

        self.spin_line_Action = tk.Spinbox(self.frame1,
                                           from_=1,
                                           to=self.linecount,
                                           increment=1,
                                           width=5)
        # Clear the spinbox
        self.spin_line_Action.delete(0, "end")
        # Set default value to 3
        self.spin_line_Action.insert(0, 3)
        # Disabled until user selects radio_action_location_Line
        self.spin_line_Action.config(state="disabled")
        self.spin_line_Action.grid(row=4, column=1,
                                   sticky=tk.W, pady=(10, 0))

        self.button_insert_Action = ttk.Button(self.frame1,
                                               text="Insert Action",
                                               command=self.insert_action)
        self.button_insert_Action.grid(row=5, column=0, columnspan=4,
                                       sticky=tk.NSEW, padx=10, pady=10)

        # This frame contains the Keyvalue section of the UI
        self.frame2 = ttk.LabelFrame(master, text="Insert Keyvalue")
        self.frame2.grid(row=2, column=3, sticky=tk.NSEW, padx=10, pady=10)

        self.L4 = ttk.Label(self.frame2, text="Key")
        self.L4.grid(row=0, column=0, sticky=tk.NSEW, padx=10, pady=(10, 0))

        self.L5 = ttk.Label(self.frame2, text="Value")
        self.L5.grid(row=0, column=1, sticky=tk.NSEW, padx=10, pady=(10, 0))

        # Where the user types the key
        self.entry_key = ttk.Entry(self.frame2, width=25)
        self.entry_key.bind("<KeyRelease>", lambda event: self.on_type(event))
        self.entry_key.grid(row=1, column=0, sticky=tk.W, padx=10, pady=10)

        # Where the user types the value
        self.entry_value = ttk.Entry(self.frame2, width=25)
        self.entry_value.bind("<KeyRelease>", lambda event: self.on_type(event))
        self.entry_value.grid(row=1, column=1, sticky=tk.W, padx=10, pady=10)

        self.L6 = ttk.Label(self.frame2, text="Insert At:")
        self.L6.grid(row=2, column=0, sticky=tk.NSEW, padx=10, pady=(10, 0))

        # Corresponds to the two "Insert At:" buttons in the Keyvalue section.
        self.var_line_kv = tk.StringVar()
        self.var_line_kv.set("end")    # Set default value to "end"

        self.radio_kv_End = ttk.Radiobutton(self.frame2,
                                            text="End",
                                            variable=self.var_line_kv,
                                            value="end",
                                            command=self.toggle_spin_kv)
        # Select this button by default
        self.radio_kv_End.state(['selected'])
        self.radio_kv_End.grid(row=3, column=0,
                               sticky=tk.W, padx=10, pady=(10, 0))

        self.radio_kv_Line = ttk.Radiobutton(self.frame2,
                                             text="Line #:",
                                             variable=self.var_line_kv,
                                             value="line",
                                             command=self.toggle_spin_kv)
        self.radio_kv_Line.grid(row=4, column=0,
                                sticky=tk.W, padx=10, pady=(10, 0))

        self.spin_line_kv = tk.Spinbox(self.frame2,
                                       from_=1,
                                       to=self.linecount,
                                       increment=1,
                                       width=5)
        # Clear the spinbox
        self.spin_line_kv.delete(0, "end")
        # Set default value to 3
        self.spin_line_kv.insert(0, 3)
        # Disabled until user selects radio_kv_Line
        self.spin_line_kv.config(state="disabled")
        self.spin_line_kv.grid(row=4, column=0,
                               sticky=tk.W, padx=(80, 0), pady=(10, 0))

        self.button_insert_kv = ttk.Button(self.frame2,
                                           text="Insert Keyvalue",
                                           command=self.insert_keyvalue)
        self.button_insert_kv.state(["disabled"])
        self.button_insert_kv.grid(row=5, column=0, columnspan=3,
                                   sticky=tk.NSEW, padx=10, pady=10)

        self.separator = ttk.Separator(master, orient=tk.HORIZONTAL)
        self.separator.grid(row=5, column=0, columnspan=4,
                            sticky=tk.EW, pady=20)

        self.buttonOK = ttk.Button(master, text="OK", command=self.save_ent)
        self.buttonOK.grid(row=6, column=0, columnspan=4, pady=(0, 20))

        self.buttonCancel = ttk.Button(master, text="Cancel",
                                       command=master.destroy)
        self.buttonCancel.grid(row=6, column=3,
                               sticky=tk.E, padx=(0, 80), pady=(0, 20))

        # Window geometry
        master.update()
        self.screenwidth = master.winfo_screenwidth()
        self.screenheight = master.winfo_screenheight()
        master.geometry("%dx%d" % (self.screenwidth/2.5,
                                   self.screenheight/1.5))


class SearchBox(tk.Frame):
    def __init__(self, parent, widget, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)

        self.widget = widget

        self.selection_count = 0
        self.indexes = []

        # Default search settings
        self.search_kwargs = {'regexp': False, 'nocase': True}

        self.widget.tag_configure("match_unselected", background="#ccff99")
        self.widget.tag_configure("match_selected", background="green")

        style = ttk.Style()
        style.configure('GrayText.TLabel', foreground="gray")

        tk.Grid.rowconfigure(self, 0, weight=1)
        tk.Grid.rowconfigure(self, 1, weight=1)
        tk.Grid.columnconfigure(self, 2, weight=1)

        self.label_count_string = tk.StringVar()
        self.label_count_string.set("")
        self.label_count = ttk.Label(self,
                                     textvariable=self.label_count_string,
                                     style='GrayText.TLabel')
        self.label_count.grid(row=0, column=0, columnspan=3, sticky="w")

        self.bool_casesens = tk.IntVar()
        self.bool_casesens.set(0)
        self.cbox_case = ttk.Checkbutton(self, text="Case Sensitive",
                                         variable=self.bool_casesens,
                                         command=self.toggle_casesens)
        self.cbox_case.grid(row=1, column=1, sticky="nsew", padx=4)

        self.bool_regex = tk.IntVar()
        self.bool_regex.set(0)
        self.cbox_regex = ttk.Checkbutton(self, text="Regex",
                                          variable=self.bool_regex,
                                          command=self.toggle_regex)
        self.cbox_regex.grid(row=1, column=0, sticky="nsew", padx=4)

        self.entry_Find = tk.Text(self, wrap="none", height=1, borderwidth=0,
                                  undo=True, maxundo=-1, autoseparators=True)
        self.entry_Find.grid(row=1, column=2, sticky="nsew", padx=(0, 5))
        # Call findall() whenever entry_Find is updated.
        self.entry_Find.bind("<<Modified>>", self.findall)

        self.button_Find = ttk.Button(self, text="Find",
                                      command=self.findnext)
        self.button_Find.grid(row=1, column=3, sticky="nsew")

        self.button_FindPrev = ttk.Button(self, text="Find Prev",
                                          command=self.findprev)
        self.button_FindPrev.grid(row=1, column=4, sticky="nsew")

        self.entry_Replace = tk.Text(self, wrap="none", height=1,
                                     borderwidth=0, undo=True, maxundo=-1,
                                     autoseparators=True)
        self.entry_Replace.grid(row=2, column=2,
                                sticky="nsew", padx=(0, 5), pady=(4, 0))

        self.button_Replace = ttk.Button(self, text="Replace",
                                         command=self.replace)
        self.button_Replace.grid(row=2, column=3, sticky="nsew")

        self.button_ReplaceAll = ttk.Button(self, text="Replace All",
                                            command=self.replaceall)
        self.button_ReplaceAll.grid(row=2, column=4, sticky="nsew")

        self.button_Close = tk.Button(self, text="×", width=4, borderwidth=0,
                                      command=self.exit)
        self.button_Close.grid(row=1, column=5, rowspan=2, sticky="e")

    def toggle_casesens(self):
        if self.bool_casesens.get() == 1:
            self.search_kwargs['nocase'] = False
        else:
            self.search_kwargs['nocase'] = True
        self.findall()

    def toggle_regex(self):
        if self.bool_regex.get() == 1:
            self.search_kwargs['regexp'] = True
        else:
            self.search_kwargs['regexp'] = False
        self.findall()

    def findall(self, event=None):
        # Reset the <<Modified>> flag for entry_Find
        self.entry_Find.edit_modified(False)

        self.indexes = []
        self.widget.tag_remove("match_unselected", "0.0", tk.END)
        self.widget.tag_remove("match_selected", "0.0", tk.END)
        phrase_find = self.entry_Find.get("1.0", "end-1c")
        if not phrase_find or phrase_find == "":
            self.label_count_string.set("")
            return 'break'
        index_start = "1.0"
        countvar = tk.IntVar()
        while True:
            try:
                index_start = self.widget.search(phrase_find, index_start,
                                                 tk.END, count=countvar,
                                                 **self.search_kwargs)
            except:
                break
            if not index_start:
                break
            if index_start == "":
                break
            if countvar.get() == 0:
                break
            index_end = '%s+%dc' % (index_start, countvar.get())
            self.widget.tag_add("match_unselected", index_start, index_end)
            self.indexes.append((index_start, index_end))
            try:
                self.widget.tag_add("match_selected",
                                    self.indexes[self.selection_count][0],
                                    self.indexes[self.selection_count][1])
            except IndexError:
                pass
            index_start = self.widget.index(index_end)
        self.label_count_string.set("%d matches" % len(self.indexes))

    def findnext(self):
        if self.indexes == []:
            pass
        else:
            outofrange = False
            try:
                self.indexes[self.selection_count+1][0]
            except IndexError:
                outofrange = True
                self.selection_count = 0
            if not outofrange:
                self.selection_count += 1
            self.widget.tag_remove("match_selected",
                                   "0.0", tk.END)
            self.widget.tag_add("match_selected",
                                self.indexes[self.selection_count][0],
                                self.indexes[self.selection_count][1])
            self.widget.see(self.indexes[self.selection_count][0])
            self.label_count_string.set("%d/%d matches" % (
                self.selection_count + 1,
                len(self.indexes))
                )

    def findprev(self):
        if self.indexes == []:
            pass
        else:
            outofrange = False
            try:
                self.indexes[self.selection_count-1][0]
            # When we go out of range, go back to the end of self.indexes.
            except IndexError:
                outofrange = True
                self.selection_count = len(self.indexes)-1
            # selection_count goes back to the end if it goes to 0 or below.
            if not outofrange:
                if self.selection_count < 1:
                    self.selection_count = len(self.indexes)-1
            # if selection_count is positive, -= 1.
                else:
                    self.selection_count -= 1
            self.widget.tag_remove("match_selected",
                                   "0.0", tk.END)
            self.widget.tag_add("match_selected",
                                self.indexes[self.selection_count][0],
                                self.indexes[self.selection_count][1])
            self.widget.see(self.indexes[self.selection_count][0])
            self.label_count_string.set("%d/%d matches" % (
                self.selection_count + 1,
                len(self.indexes))
                )

    def replace(self):
        outofrange = False
        phrase_replace = self.entry_Replace.get("1.0", "end-1c")
        if self.selection_count is None:
            self.selection_count = 0
        try:
            self.widget.delete(self.indexes[self.selection_count][0],
                               self.indexes[self.selection_count][1])
        except IndexError:
            outofrange = True
        if not outofrange:
            self.widget.insert(self.indexes[self.selection_count][0],
                               phrase_replace)

    def replaceall(self):
        index_start = "1.0"
        phrase_find = self.entry_Find.get("1.0", "end-1c")
        phrase_replace = self.entry_Replace.get("1.0", "end-1c")
        if self.selection_count is None:
            self.selection_count = 0
        if not phrase_find:
            return
        countvar = tk.IntVar()
        while True:
            index_start = self.widget.search(phrase_find, index_start,
                                             tk.END, count=countvar,
                                             **self.search_kwargs)
            if not index_start:
                break
            index_end = '%s+%dc' % (index_start, countvar.get())
            self.widget.delete(index_start, index_end)
            self.widget.insert(index_start, phrase_replace)
            index_start = self.widget.index(
                '%s+%dc' % (
                    index_start,
                    countvar.get()+len(phrase_replace)-len(phrase_find)
                    )
            )

    def exit(self):
        self.widget.tag_remove("match_unselected", "0.0", tk.END)
        self.widget.tag_remove("match_selected", "0.0", tk.END)
        self.master.event_generate("<<SearchBoxExit>>", when="tail")
        self.destroy()


class LumpViewer:

    def on_scrollbar_lumpviewer(self, *args):
        self.textbox_lumpviewer.yview(*args)
        self.linenumbers_lumpviewer.redraw()

    def on_text_scroll_lumpviewer(self, *args):
        self.scrollbar_y_lumpviewer.set(*args)
        self.on_scrollbar_lumpviewer("moveto", args[0])

    def on_item_select(self, event):
        if self.warningstree.item(self.warningstree.selection(), "value") != "":
            index = "%s.0" % self.warningstree.item(self.warningstree.selection(), "value")
            self.textbox_lumpviewer.see(index)

    def on_searchbox_exit(self, event):
        self.searchbox_exists = False

    def __init__(self, master):
        self.searchbox_exists = False
        self.master = master
        self.master.withdraw()

        self.bsp_path = filedialog.askopenfilename(
            title="Open .bsp",
            filetypes=[("Source map file (.bsp)", "*.bsp")]
            )
        # If user chooses a bsp, make the GUI.
        if self.bsp_path:
            tk.Grid.rowconfigure(self.master, 1, weight=1)
            tk.Grid.columnconfigure(self.master, 1, weight=1)
            self.master.wm_deiconify()
            self.master.title("BSP Lump - %s" % self.bsp_path)

            self.lumpviewer_menubar = tk.Menu(self.master)
            self.master.config(menu=self.lumpviewer_menubar)
            self.menubar_view = tk.Menu(self.lumpviewer_menubar, tearoff=False)
            self.menubar_view.add_command(label="Find      Ctrl+F",
                                          command=lambda: self.create_searchbox(event=None))
            self.lumpviewer_menubar.add_cascade(label="View", menu=self.menubar_view)

            self.textbox_lumpviewer = tk.Text(self.master)
            self.textbox_lumpviewer.grid(row=0, column=1, rowspan=2, sticky="nsew", pady=10)
            self.scrollbar_y_lumpviewer = ttk.Scrollbar(self.master,
                                                        command=self.on_scrollbar_lumpviewer)
            self.scrollbar_y_lumpviewer.grid(row=0, column=2, rowspan=2,
                                             sticky="nsew", padx=(0, 5), pady=10)
            self.textbox_lumpviewer.configure(yscrollcommand=self.on_text_scroll_lumpviewer)
            self.linenumbers_lumpviewer = TextLineNumbers(self.master, width=40,
                                                          borderwidth=0, takefocus=0)
            self.linenumbers_lumpviewer.attach(self.textbox_lumpviewer)
            self.linenumbers_lumpviewer.grid(row=0, column=0, rowspan=2, sticky="nsew", pady=10)
            self.linenumbers_lumpviewer.configure(yscrollcommand=self.on_text_scroll_lumpviewer)

            self.warning = tk.StringVar()
            self.warning.set("0 warnings")
            self.warningslabel = ttk.Label(self.master, textvariable=self.warning)
            self.warningslabel.grid(row=0, column=3, sticky="nsew", padx=40, pady=10)

            self.warningstree = ttk.Treeview(self.master, show="tree", selectmode='browse')
            self.warningstree.tag_configure("red", background="#f25c66")
            self.warningstree.grid(row=1, column=3, sticky="nsew", padx=40, pady=10)
            self.warningstree.bind("<<TreeviewSelect>>", self.on_item_select)

            with open(self.bsp_path, "rb") as bsp_file:
                self.bsp = bsp_file.read()
                REALLY_BAD_THINGS = ["jail", "weapon", "secret", "game_player_equip",
                                     "trigger_hurt", "env_shake", "func_button", "func_rot_button",
                                     "env_hudhint", "game_text", "point_servercommand",
                                     "player_speedmod"]
                warningscount = 0
                try:
                    self.lump = bspmodule.BSP(self.bsp)
                    # Insert the ent lump into the textbox
                    self.textbox_lumpviewer.insert("1.0",
                                                   self.lump.get_lump(0).decode(errors="ignore"))
                    # Look for potentially problematic entities with bspsearch
                    self.problem_searcher = bspsearch.BSPsearch(
                        self.lump.get_lump(0).decode(errors="ignore"))
                    for key, val in self.problem_searcher.search().items():
                        if val != []:
                            warningscount += 1
                            # Add problem entities to the tree and add the
                            # linenumbers they occur on as child items.
                            # Make the tree entry red if it's a REALLY_BAD_THING.
                            if key in REALLY_BAD_THINGS:
                                warning_parent = self.warningstree.insert('', 'end',
                                                                          text=key,
                                                                          tag="red")
                                child_tag = "red"
                            else:
                                warning_parent = self.warningstree.insert('', 'end',
                                                                          text=key)
                                child_tag = ""
                            for linenum in val:
                                self.warningstree.insert(warning_parent, 'end',
                                                         text="Line %d" % linenum,
                                                         value=linenum, tag=child_tag)
                            if warningscount == 1:
                                self.warning.set(
                                    "1 warning (double click to view)")
                            else:
                                self.warning.set(
                                    "%d warnings (double click to view)" % warningscount)
                except Exception as e:
                    tk.messagebox.showerror(
                        "",
                        "Could not open BSP. Error:\n%s" % e)

            self.master.bind("<<SearchBoxExit>>", self.on_searchbox_exit)
            self.master.bind("<Control-f>", self.create_searchbox)
            self.textbox_lumpviewer.configure(state="disabled")

    def create_searchbox(self, event):
        # Only make a new SearchBox is there isn't already one
        if not self.searchbox_exists:
            self.searchbox_exists = True
            self.searchbox = SearchBox(self.master, self.textbox_lumpviewer)
            self.searchbox.grid(row=2, column=0, columnspan=3, sticky="nsew", pady=10)
            self.searchbox.entry_Replace.destroy()
            self.searchbox.button_Replace.destroy()
            self.searchbox.button_ReplaceAll.destroy()
            self.master.bind("<Escape>", lambda x: exit_searchbox())

        def exit_searchbox():
            self.searchbox_exists = False
            self.searchbox.exit()


root = tk.Tk()
if 'win' in tk.sys.platform:
    root.state('zoomed')
RUN = MainGUI(root, file=None)
root.mainloop()
